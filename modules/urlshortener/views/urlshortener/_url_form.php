  <h4>Shorten Your URL:</h4>
  <form data-method="post" data-remote="true" method="POST" action="/create" data-type="json" id="shorten_form">
    <label for="opts[url]">URL to shorten</label>
    <input type="text" placeholder="http://your-long-url.com" name="opts[url]" class="text_input" value="" id="original_url" required="required">

    <label for="opts[path]">Custom path</label>
    <input type="text" placeholder="(optional)" name="opts[path]" class="short_input" maxlength="20" value="" id="path">

    <input type="hidden" name="submit" value="Shorten">
    <input type="submit" name="submit" value="Shorten" class="action submit_button" data-disable-with="Shortening...">

    <span class="edgetoedge info form_error"></span>
  </form>
