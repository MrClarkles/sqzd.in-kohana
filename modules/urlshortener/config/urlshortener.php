<?php defined('SYSPATH') or die('No direct access allowed.');

return array
(
    'reserved_urls' => array(),
    'url_table'     => 'urls',
    'stats_table'   => 'stats',
    'base_url'      => "http://".$_SERVER['SERVER_NAME'],
    'path_exclusions' => array('static','api','index','latest_urls','new','admin','create','update','delete','destroy')
);
