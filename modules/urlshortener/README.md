#URL Squeezer
---
##URLS:
Shorten 'em, re-bigulate 'em, track 'em, provide a little reporting, administer 'em


###HOWTO:
  - Create tables in a new or existing database using db_schema.sql.
  - Copy config/urlsqueezer.php into application/config/ and edit values.
  - Add 'urlsqueezer'  => MODPATH.'urlsqueezer' to your modules array in bootstrap.php
