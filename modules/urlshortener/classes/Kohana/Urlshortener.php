<?php defined('SYSPATH') or die('No direct script access.');

class Kohana_Urlshortener {

    protected $base_url;        // Base URL
    protected $url_table;       // URL Table
    protected $stats_table;     // Stats Table
    protected $submitted_url;   // URL to squeeze
    protected $code;            // Short code
    protected $short_url;       // Squeezed URL
    protected $status;          // errors


    public static function factory()
    {
        return new Urlshortener();
    }

    public function __construct()
    {
        $config             = Kohana::$config->load('urlshortener')->as_array();
        $this->url_table    = $config['url_table'];
        $this->stats_table  = $config['stats_table'];
        $this->base_url     = $config['base_url'];
    }

    public function create_form()
    {
        $view = View::factory("shortener/create");
        if ($this->status == "error"):
            $view->bind("error", $this->status);
        elseif ($this->short_url):
            $view->bind("url", $this->short_url);
        endif;
        return $view->render();
    }

    public function shorten($url)
    {
        $this->status = '';

        if ( $this->is_valid_url($url) ):

            $result = ORM::factory(Inflector::singular($this->url_table))
                    ->where("url", "=", $url)
                    ->find();

            if ($result->loaded())
            {
                $this->short_url = $this->base_url.$result['code'];
                return $this->short_url;
            }

            $this->code = $this->create_random_code();
            $this->submitted_url = $url;

            try
            {
                $rec = ORM::factory(Inflector::singular($this->url_table));
                $rec->original_url = $url;
                $rec->path = $this->code;
                $rec->created_at = date("Y-m-d H:i:s");
                $rec->save();
            }
            catch (ORM_Validation_Exception $e)
            {
                $errors = $e->errors();
                $this->status = "error";
            }


            if ( length($this->status) > 0 ):
                return false;
            else:
                $this->short_url = $this->base_url.$this->code;
                return $this->short_url;
            endif;

        else:
            $this->status = "error";
            return false;
        endif;

    }


    /**
     * Generates a random code
     *
     * @return string code
     */
    private function create_random_code()
    {
        $code = "";
        $character_set = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

        for ($i = 1; $i <= 6; $i++):
            $code .= $character_set[rand(0, strlen($character_set) - 1)];
        endfor;

        // Check to see if this code exists already in the database
        $result = ORM::factory(Inflector::singular($this->url_table))
                    ->select("code")
                    ->where("code", "=", $code)
                    ->find();

        // If so, create a new code
        if ( $result->loaded() && ($result->count() > 0)):
            return $this->create_random_code();
        endif;

        return $code;

    } // function create_random_code()


    /**
     * Check if a URL is well formed
     *
     * @param string url
     * @return boolean
     */
    private function is_valid_url($url)
    {

        return preg_match('/^[a-zA-Z]+[:\/\/]+[A-Za-z0-9\-_]+\\.+[A-Za-z0-9\.\/%&=\?\-_]+$/i', $url);
        //return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);

    }  // function is_valid_url()


    /**
     * Looks up short code and links user to original URL
     *
     * @param string code
     * @return 302 redirect on success
     * @return boolean on fail
     */
    public function lengthen($code)
    {
        // Check to see if valid code
        if ( preg_match("/^[0-9a-zA-Z]{6}$/", $code) ):

            $result = ORM::factory(Inflector::singular($this->url_table))
                    ->where("code", "=", $code)
                    ->find();

            if ( $result->loaded() && $result->count() > 0  ):
                $row = $result->current();
                Request::current()->redirect( $row['url']) ;
                exit;
            else:
                return false;
            endif;
        else:
            return false;
        endif;
    } // function lengthen_url

    public function getStaticFilenames($path='')
    {
        //return a list of files in "static" dir
        $static_path = ($path !== '') ? $path : 'views/urlshortner';
        $files = Kohana::find_file($static_path, 'views/urlshortner');
    }


  public function latest_urls($limit=10)
  {
    $urls = array();

    $results = ORM::factory(Inflector::singular($this->url_table))
        ->limit($limit)
        ->order_by('created_at', 'desc')
        ->find_all()
        ->as_array();

    if (count($results) > 0) {
        foreach( $results as $item) {
            $urls[] = $item->as_array();
        }
    }
    return $urls;
  }


}
