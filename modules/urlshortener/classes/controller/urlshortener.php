<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Urlshortener extends Controller {


    public function action_lengthen()
    {
        $code = $this->request->param("code");
        $shortener = Urlshortener::factory();
        $shortener->lengthen($code);

    }

    public function action_latest()
    {
        //we're only returning some html here
        $shortener = Urlshortener::factory();
        $num = $this->request->param('num');

        $limit = ($num > 0) ? $num : 10;
        $latest_urls = $shortener->latest_urls($limit);

        $this->auto_render = false;

        $content = Partial::factory('urlshortener/latest_url')->collection($latest_urls)->render();
        $content = "<ul class=\"edgetoedge plastic\">\n".$content."</ul>\n";
        $this->response->body($content);
    }

    public function action_create()
    {

        $shortener = Urlshortener::factory();

        if (isset($_REQUEST['api']) && $_REQUEST['api'] == true):

            $this->auto_render = false;
            echo $shortener->shorten($_REQUEST['url']);

        else:

            if ( isset($_POST['url']) ):
                $shortener->shorten($_POST['url']);
            endif;
            $this->content = $shortener->render();

        endif;
    }

    public function action_static()
    {

        // requested static page
        $page = $this->request->param('page');
        $this->body_class = __($page);
    }

} // End Shortener
