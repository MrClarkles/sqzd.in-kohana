<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_Url extends ORM {

  protected $_table_name = 'urls';
  protected $_has_many = array('stat' => array());

  public function rules()
  {
    return array(
      'original_url' => array(
        array('not_empty'),
        array('min_length', array(':value', 2) ),
        array('max_length', array(':value', 2024) ),
        array('url'),
      ),
      'path' => array(
        array('min_length', array(':value', 2) ),
        array('max_length', array(':value', 20) ),
        array('regex', array(':value', '/^[-_a-z0-9]+$/i') ),
      ),
    );
  }

  public function labels()
  {
    return array(
      'original_url' => 'url',
      'path'         => 'custom path',
      'hits'         => 'hits',
      'title'        => 'title',
    );
  }

  public function filters()
  {
    return array();
  }

  public function random_path($url)
  {
    $random_path = Base62::random(path_size());
    return $random_path;
  }

  public function path_size()
  {
    $num_recs = ORM::count_all();
    return (length($num_recs) - 1);
  }
}


/*
  validates :original_url,
            :presence => true,
            :uniqueness => { :case_sensitive => false, :message => "This URL is already in our database" },
            :length => { :maximum => 2024 },
            :format => { :with => URL_REGEXP, :message => "Invalid URL" }


  validates :path,
            :allow_blank => true,
            :uniqueness => { :case_sensitive => false, :message => "Custom path has already been taken" },
            :length => { :maximum => 20, :minimum => 2, :message => "Custom path must be between 2 and 20 characters"},
            :format => { :with => /^[-_a-z0-9]+$/i, :message => "Invalid Path" }


  def self.random_path
    Anybase::Base62.random(path_size).downcase
  end

  def self.path_size
    Url.count.to_s.length.to_i-1
  end

  def self.delete!
    self.destroy
  end

end
*/
