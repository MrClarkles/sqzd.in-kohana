<?php defined('SYSPATH') or die('No direct script access.');

  class Controller_Static extends Controller_Application {

      public function action_index()
      {
        // Get the name of our requested page
          $page = $this->request->param('page');
          $this->template->body_class = __($page);

          $this->template->meta_title = __('Professional Website Development and Maintenance Services');
          $this->template->meta_description = __('W3Geekery offers web design and development services, site maintenance services, domain name, website, and email hosting, all with a personal touch.');
          $this->template->meta_keywords = __('Web design, web development, web hosting, site maintenance, geekery, jquery, ajax, freelance');

          $this->template->footer_scripts = array(
            '/media/js/jquery.qtip.min.js',
            '/media/js/jquery.sparkline.min.js',
          );
          $this->template->body_class = __('index_wrap');

          $shortener = Urlshortener::factory();
          $latest_urls = $shortener->latest_urls(10);

          $content = Partial::factory('urlshortener/latest_url')->collection($latest_urls)->render();
          $content = "<ul class=\"rounded\">\n".$content."</ul>\n";

          $this->template->content = View::factory('static/index')
            ->bind('latest_urls', $content)
            ->render();

      }

  }
