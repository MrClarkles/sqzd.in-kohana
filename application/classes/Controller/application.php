<?php
 defined('SYSPATH') or die('No direct script access.');

 class Controller_Application extends Controller_Template
  {
     public $template = 'layouts/application';

     /**
      * Initialize properties before running the controller methods (actions),
      * so they are available to our action.
      */

     public function before()
      {

/*
Log::instance()->add(Log::NOTICE, 'Action: '.$this->request->action());
Log::instance()->add(Log::NOTICE, 'Page: '.$this->request->param('page'));
*/
         // Run anything that need ot run before this.
         parent::before();

         if($this->auto_render)
          {
            // Initialize empty values
            $this->template->meta_title       = '';
            $this->template->meta_keywords    = '';
            $this->template->meta_description = '';
            $this->template->meta_copyright   = '';
            $this->template->header           = '';
            $this->template->body_class       = '';
            $this->template->content          = '';
            $this->template->footer           = '';
            $this->template->styles           = array();
            $this->template->scripts          = array();
            $this->template->footer_scripts   = array();
          }
      }


     /**
      * Fill in default values for our properties before rendering the output.
      */
     public function after()
      {
         if($this->auto_render)
          {
             // Define defaults
             $styles = array(
              '/media/css/screen.css'           => 'screen, projection',
              '/media/css/base.css'             => 'screen, projection',
              '/media/css/jqtouch.css'          => 'screen',
              '/media/css/themes/jqt/theme.css' => 'screen, projection',
              '/media/css/jquery.qtip.css'      => 'screen',
              '/media/css/print.css'            => 'print',
              );
             $scripts = array(
              'http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js',
              '/media/js/jqtouch.js',
              );
             $footer_scripts = array(
              '/media/js/jqtouch-jquery.js',
              '/media/js/application.js',
              );
             // Add defaults to template variables.
             $this->template->styles  = array_merge($this->template->styles, $styles);
             $this->template->scripts = array_merge($this->template->scripts, $scripts);
             $this->template->footer_scripts = array_merge($this->template->footer_scripts, $footer_scripts);
           }

         // Run anything that needs to run after this.
         parent::after();
      }
 }
