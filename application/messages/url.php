<?php defined('SYSPATH') or die('No direct access allowed.');

return array(
    'original_url' => array(
        'not_empty' => 'You must provide a url.',
        'min_length' => 'The url must be at least :param2 characters long.',
        'max_length' => 'The url must be less than :param2 characters long.',
        'url' => 'You must provide a valid url.',
    ),
    'path' => array(
        'min_length' => 'The path must be at least :param2 characters long.',
        'max_length' => 'The path must be less than :param2 characters long.',
        'regex' => 'Custom path can only contain letters, numbers, "-", and "_".'
    ),
);
