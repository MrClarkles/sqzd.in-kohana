<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */

Route::set('static', '<page>')
  ->defaults(array(
    'controller' => 'static',
    'action'     => 'index',
    'page'       => 'index'
));

Route::set('default', '(<controller>(/<action>(/<id>)))')
  ->defaults(array(
    'controller' => 'static',
    'action' => 'index',
    'page' => 'index'
));

Route::set('error', 'error/<action>(/<message>)', array('action' => '[0-9]++', 'message' => '.+'))
  ->defaults(array(
    'controller' => 'error_handler'
));

