<?php defined('SYSPATH') or die('No direct script access.');

//  expand an incoming URL
/*
  match '/api/latest_urls' => 'api#latest_urls',  :as => :latest_urls
  match '/api/create'      => 'api#create',  :via => :post, :as => :create

  match '/admin'           => 'admin#index'

  match '/:path_plus'   => 'api#show',   :via => :get, :requirements => {:path_plus => /\+$/}
  match '/:path'   => 'api#expand', :via => :get
  match '/'        => 'api#index',  :via => :get
*/


Route::set('urlshortener_expand', '<path>+',
    array('path' => '[a-zA-Z0-9]+\+$', ))
    ->defaults(array(
        'controller' => 'urlshortener',
        'action'     => 'expand',
    ));

// REST Routes for the URL Squeezer for administration
Route::set('urlshortener_create', 'create')
    ->defaults(array(
        'controller' => 'urlshortener',
        'action'     => 'create',
    ));

Route::set('urlshortener_show', 'show/(<code>)')
    ->defaults(array(
        'controller' => 'urlshortener',
        'action'     => 'show',
    ));

Route::set('urlshortener_update', 'update/<id>')
    ->defaults(array(
        'controller' => 'urlshortener',
        'action'     => 'update',
    ));

Route::set('urlshortener_destroy', 'destroy/<id>')
    ->defaults(array(
        'controller' => 'urlshortener',
        'action'     => 'destroy',
    ));

Route::set('urlshortener_latest', 'latest/(<num>)')
    ->defaults(array(
        'controller' => 'urlshortener',
        'action'     => 'latest',
        'num'        => 10,
    ));
