<div id="home" class="main_content current">
  <div class="toolbar">
    <?php echo Partial::factory('layouts/header'); ?>
  </div>

  <div class="form_wrap">
  <?php echo Partial::factory('urlshortener/url_form'); ?>
  </div>

  <div id="latest_urls" class="clearfix">
    <?php echo $latest_urls; ?>

    <ul class="individual">
      <li class="prev_button ghosted"><a href="#">&laquo; Previous</a></li>
      <li class="next_button"><a href="#">Next &raquo;</a></li>
    </ul>

  </div>

  <div id="page_footer">
    <?php echo Partial::factory('layouts/footer'); ?>
  </div>
</div>

