<div id="privacy" class="main_content">
  <div class="toolbar">
    <?php echo Partial::factory('layouts/header'); ?>
  </div>

  <div class="static_wrap info clearfix">
    <h1>Privacy Policy</h1>
    <p>We do not collect any personally identifying information from users of this website. We store only the technical information necessary to provide our URL shortening service, such as the original long URLs. We may also store the IP addresses of computers using the sqzd.in service (and similar usage information such as web browser/resolution) for the sole purposes of identifying abuse of the service and tracking anonymous usage trends. This information will not be made available to third parties.</p>

    <p>URLs shortened by our website are not private and should not be treated as such. Third parties could easily guess the short URL that you are using, so you should not use sqzd.in to link to sensitive or secure data.</p>
  </div>

  <div id="page_footer">
    <?php echo Partial::factory('layouts/footer'); ?>
  </div>
</div>


