  <form data-method="post" data-remote="true" method="POST" action="/api/create" data-type="json" id="shorten_form">
    <input type="text" placeholder="URL to shorten" name="opts[url]" value="" id="original_url" required="required">
    <input type="text" placeholder="Custom path (optional)" name="opts[path]" maxlength="20" value="" id="path">

    <input type="submit" name="create_submit" value="Shorten" class="button submit_button" data-disable-with="Shortening...">
    <!--
    <input type="submit" id="submit_create" name="submit" value="create">
    -->

    <span class="form_error"></span>
  </form>
