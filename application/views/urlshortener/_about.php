<div id="about" class="main_content">
  <div class="toolbar">
    <%= @header %>  
  </div>

  <div class="static_wrap info clearfix">
    <h1>About Us</h1>
    <p>We're seeking to provide a simple but useful URL shortening service for you. Thanks for visiting!</p>

    <h2>Contact Info</h2>
    <p>If you would like to have a url removed, please send an email to the sqzd.in Link Administrator</p>
  </div>

  <div id="page_footer">
    <%= @footer %>  
  </div>
</div>


