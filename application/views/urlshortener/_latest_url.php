    <li class="arrow" title="<?php echo ($latest_url['title'] !== '') ? $latest_url['title'] : $latest_url['original_url']; ?>">
      <span class="path_link"><?php echo HTML::anchor($latest_url['path'], $latest_url['path'], array('target' => '_blank')); ?></span>
      <span class="stats"><?php echo HTML::anchor( $latest_url['path'].'+', "more stats", array('class' => "action show_stats") ); ?></span>
      <span class="hit_count"><?php echo $latest_url['hits']." ".Inflector::plural('click', $latest_url['hits']); ?></span>
    </li>
