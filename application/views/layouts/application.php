<?php defined('SYSPATH') or die('No direct script access.'); ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6 lte7 lte8 lte9"><![endif]-->
<!--[if IE 7 ]><html class="ie ie7 lte7 lte8 lte9"><![endif]-->
<!--[if IE 8 ]><html class="ie ie8 lte8 lte9"><![endif]-->
<!--[if IE 9 ]><html class="ie ie9 lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class=""><!--<![endif]-->
<head>
  <title><?php echo $meta_title ?> | W3Geekery</title>

  <meta name="description" content="<?php echo $meta_description ?>" />
  <meta name="keywords" content="<?php echo $meta_keywords ?>" />
  <meta name="copyright" content="Copyright 2010 - <?php echo date('Y'); ?> Sqzd.in. All Rights Reserved." />

  <link rel="shortcut icon"  href="favicon.ico" />

<?php foreach ($styles as $file => $type) echo "    ".HTML::style($file, array('media' => $type))."\n"; ?>

<?php foreach ($scripts as $file) echo "    ".HTML::script($file)."\n"; ?>

  <meta http-equiv="imagetoolbar" content="false" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="user-scalable=no, width=device-width" />
</head>
<body class="<?php echo $body_class; ?>">
  <div id="jqt">
    <?php echo $content; ?>
  </div>
  <?php foreach ($footer_scripts as $file) echo "    ".HTML::script($file)."\n"; ?>
</body>
</html>
