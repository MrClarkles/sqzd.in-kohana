<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'default' => array(
        'type'       => 'mysql',
        'connection' => array(
            'hostname'   => 'localhost:8889',
            'username'   => 'root',
            'password'   => 'root',
            'database'   => 'i83e_sqzd_in',
            'persistent' => FALSE,
        ),
        'table_prefix' => '',
        'charset'      => 'utf8',
        'caching'      => FALSE,
        'profiling'    => FALSE,
    ),
);
