<?php defined('SYSPATH') or die('No direct script access.');

return array(
  // Use the cache directory on development because sessions are not important
  'save_path' => '/Users/clark/Sites/PROJECTS/sqzd.in/application/cache',
  'database' => array(
    'name'  => 'session_database',
    'group' => 'default',
    'table' => 'sessions',
    'encrypted' => TRUE,
  ),
  'cookie' => array(
    'name' => 'session_cookie',
    'group' => 'default',
    'encrypted' => TRUE,
    'lifetime' => 43200,
  ),
  'native' => array(
    'name' => 'session_native',
  ),
);
