<?php defined('SYSPATH') OR die('No direct access allowed.');

//Developerbar::factory()->enabled(true);

return array(
    'base_url'    => '/',
    'index_file'  => '',
    'charset'     => 'utf-8',
    'cache_dir'   => APPPATH.'cache',
    'errors'      => TRUE,
    'profile'     => FALSE,
    'caching'     => FALSE,
);
