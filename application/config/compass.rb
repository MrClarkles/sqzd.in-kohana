# create a cron job to run
# compass watch -c application/config/compass.rb
# from your project's physical root dir

http_path = "/"
css_dir = "public/media/css"
sass_dir = "application/stylesheets"
images_dir = "public/media/images"
javascripts_dir = "public/media/js"
fonts_dir = "public/media/fonts"
output_style = :expanded
line_comments = false

# output_style = :expanded or :nested or :compact or :compressed
