$.fn.clearForm = function() {
  return this.each(function() {
    var type = this.type, tag = this.tagName.toLowerCase();
    if (tag == 'form')
      return $(':input',this).clearForm();
    if (type == 'text' || type == 'password' || tag == 'textarea')
      this.value = '';
    else if (type == 'checkbox' || type == 'radio')
      this.checked = false;
    else if (tag == 'select')
      this.selectedIndex = -1;
});
};

$(function() {

  $.ajaxSetup({
    context: $("#content_wrap"),
    type: "POST"
  });


  $('.action').live('click',function(e) {
    e.preventDefault();
    var url = encodeURIComponent($(this).attr('href'));
    try {
      jQT.goTo(url,'slideleft');
    } catch (error) {
      //error
    }
    return false;
  });


  var Flash = function(msg,key,length) {
    if (msg) {
      var key  = (typeof key  == "undefined")?'notice':key;
      var length = (typeof length == "undefined")?2000:Number(length);

      if ($('#flash_message').length < 1) {
        $('body').prepend('<div id="flash_message"></div>');
      }

      var $f = $("#flash_message");
      var c = '<span class="close" onclick="$(\'#flash_message\').slideUp();return false;">[close]</span>';
      $f.removeClass('notice error').addClass(key).empty().append("<p>"+msg+"</p>").prepend(c).slideDown().delay(length).slideUp();
    }
    return false;
  };

  $('#shorten_form').live('ajax:complete', function(o,r) {
    if (r.responseText) {
      var j = $.parseJSON(r.responseText);

      if (typeof j.errors != "undefined") {
        var msg = '';
        $.each(j.errors,function(i,error) {
          $("#"+i).addClass('field_error');
          msg += error+"<br />";
        });
        $('.form_error').empty().append(msg).slideDown();
      } else {
        $('.field_error').removeClass('field_error');
        $('.form_error').empty().slideUp();
        Flash("Successfully added your URL",'notice');
        $('#shorten_form').clearForm();
        refresh_latest_urls();
      }

    }
    return false;
  });

  function refresh_latest_urls() {
    $.ajax({
      url: '/latest',
      dataType: 'html',
      type: 'GET',
      complete: function(r) {
        $('#latest_urls').empty().append(r.responseText);
        $('#latest_urls ul li[title]').qtip({
          style: {classes:'ui-tooltip-blue ui-tooltip-shadow'},
          title: {text:'Original URL:'},
          position: {
            my: 'bottom center',
            at: 'center',
            target: 'mouse'
          }
        });
      }
    });
  }

  //$(':input:visible:enabled:first').focus();


  var jQT = new $.jQTouch({
    icon: 'jqtouch.png',
    statusBar: 'black-translucent',
    formSelector: '.ajaxform'
  });


});
